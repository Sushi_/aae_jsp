'use strict';

(function(api) {
	//var ERRNO_INVALID_USERNAME = 1;
	//var ERRNO_INVALID_PASSWORD = 2;
	//var ERRNO_DUPLICATE_USERNAME = 3;

	var form = document.querySelector('main form');
	$(form).floatLabel();
	
	var usernameInput = form.querySelector('input[name=username]');
	var passwordInput = form.querySelector('input[name=password]');

	api.bindForm(form, {
		preSubmit: function(button, overwrite) {
			if (button.name === 'register') {
				overwrite.action = 'account';
			} else {
				overwrite.action = 'login';
			}

			return true;
		},
		dataHandler: function(user) {
			api.logIn(user);
			api.redirect('/');
		},
		errnoHandlers: {
			1: {
				field: usernameInput,
				message: 'Invalide',
			},
			2: {
				field: passwordInput,
				message: 'Invalide',
			},
			3: {
				field: usernameInput,
				message: 'Déjà utilisé',
			}
		}
	});
})(api);