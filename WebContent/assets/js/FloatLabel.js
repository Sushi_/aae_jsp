'use strict';

/**
 * @author Guylian Cox - ephys.github.io
 */
(function($) {
	var supportPlaceholder = document.createElement('input').placeholder !== void (0);
	
	$.fn.floatLabel = function() {
		if (!supportPlaceholder) {
			return;
		}

		var labelize = function(form) {
			var $labelContainers = $(form).children('div.float_label');

			$labelContainers.each(function() {
				var $container = $(this);

				var $label = $(this).children('label');
				var $input = $(this).children('input, textarea');
				var input = $input[0];
				var label = $label[0];
				
				input.placeholder = label.textContent;

				var onKeyUp = function() {
					if (input.value === '') {
						$container.addClass('hide-label');
					} else {
						$container.removeClass('hide-label');
					}
				};

				this.addEventListener('keyup',onKeyUp);

				onKeyUp();
				$container.addClass('hide-label');
			});
		};

		this.each(function() {
			if (!(this instanceof HTMLFormElement)) {
				throw '$.floatLabel constructor requires an HTMLFormElement as a parameter';
			}

			labelize(this);
		});

		return this;
	};
})(jQuery);