'use strict';

(function(api) {
	//var ERRNO_CREATE_INVALID_NAME = 1;

	var createForm = document.querySelector('#form_create');
	if (createForm !== null) {
		$(createForm).floatLabel();
		
		var nameInput = createForm.querySelector('input[name=name]');

		api.bindForm(createForm, {
			dataHandler: function() {
				api.redirect('/play');
			},
			errnoHandlers: {
				1: function() {
					alert('Un jeu est déjà en cours!');
					location.reload(true);
				},
				3: {
					field: nameInput,
					message: 'Invalide',
				}
			}
		});	
	}

	var joinForms = document.querySelectorAll('.form_join');
	for (var i = 0; i < joinForms.length; i++) {
		api.bindForm(joinForms[i], {
			dataHandler: function() {
				api.redirect('/play');
			},
			errnoHandlers: {
				1: function() {
					alert('Le jeu n\'est plus joignable!');
					location.reload(true);
				}
			}
		});
	}
})(api);