'use strict';

var api = (function($) {
	var URL_BASE = (function() {
		var base = document.baseURI;
		
		if (base == null) {
			console.log(document);
			var baseTags = document.head.getElementsByTagName('base');
			if (baseTags.length === 0) {
				throw new Error('the document needs a <base> tag in the head for the api to work');
			}
			
			base = baseTags[0].href;
		}
		
		return base.substring(0, base.length - 1);
	})();

	return {
		redirect: function(path) {
			window.location = URL_BASE + path;
		},
		
		logIn: function(user) {
			sessionStorage.setItem('user', JSON.stringify(user));
		},
		
		logOut: function() {
			sessionStorage.removeItem('user');
		},
		
		isLogged: function() {
			return sessionStorage.getItem('user') !== null;
		},
		
		getLoggedUser: function() {
			var data = sessionStorage.getItem('user');
			if (data === null) return null;

			return JSON.parse(data);
		},

		callApi: function(method, path, body) {
			var self = this;
			// starts with / but not with //
			var url = path.match(/^\/[^\/]/) ? URL_BASE + path : path;
			return $.ajax({
				url: url,
				type: method,
				dataType: 'json',
				data: body || '',
				accepts: {'json': 'application/json'},
			}).fail(function(xhr, error, g) {
				if (xhr.status === 401) {
					api.redirect('/login');
				}
				
				console.warn('request failure', xhr, error, g);
			});
		},

		bindForm: function(form, overwrite) {
			if (overwrite === void 0) {
				overwrite = {};
			} else if (typeof overwrite !== 'object') {
				throw new Error('overwrite must be an object');
			}
			
			if (overwrite.errnoHandlers !== void 0) {
				// remove custom validity when typing
				for (var i in overwrite.errnoHandlers) {
					if (typeof overwrite.errnoHandlers[i] !== 'object') continue;
					
					var field = overwrite.errnoHandlers[i].field;
					if (field.nodeName === 'INPUT') {
						field.addEventListener('input', function() {
							this.setCustomValidity('');
						});
					}
				}
			}

			var api = this;
			var buttons = form.querySelectorAll('button[type=submit]');
			
			for (var i = 0; i < buttons.length; i++) {
				buttons[i].addEventListener('click', function(e) {
					e.preventDefault();

					if (this.hasAttribute('submitting')) {
						return false;
					}

					var inputs = form.querySelectorAll('button[type=submit],input[type=submit]');
					for (var i = 0; i < inputs.length; i++) {
						inputs[i].disabled = true;
					}

					this.setAttribute('submitting', 'submitting');
					var self = this;
					var done = function() {
						self.removeAttribute('submitting');

						for (var i = 0; i < inputs.length; i++) {
							inputs[i].disabled = false;
						}
					};
					
					if (typeof overwrite.preSubmit === 'function') {
						if (!overwrite.preSubmit(this, overwrite)) {
							return;
						}
					}

					var method = overwrite.method || form.method;
					var action = overwrite.action || form.getAttribute('action');
					var params = overwrite.body || $(form).serialize();
					
					console.log('Calling', method, action, 'with body', params);

					var call = api.callApi(method, action, params).always(done);
					if (typeof overwrite.dataHandler === 'function') {
						call.done(overwrite.dataHandler);
					}

					if (typeof overwrite.errorHandler === 'function') {
						call.fail(overwrite.errorHandler);
					}
					
					if (typeof overwrite.doneHandler === 'function') {
						call.always(overwrite.doneHandler);
					}

					if (overwrite.errnoHandlers !== void 0) {					
						call.fail(function(xhr) {
							var response = xhr.responseJSON;

							if (response.errno === -1) {
								console.error(response.message);
							}
							
							var errorHandler = overwrite.errnoHandlers[response.errno];
							if (errorHandler === void 0) {
								return console.error('No error handler for errno ' + response.errno + ' - ' + response.message);
							}
							
							if (typeof errorHandler === 'function') {
								errorHandler(form, xhr);
								return;
							}

							var field = errorHandler.field;
							var message = errorHandler.message || response.message;

							if (field.nodeName === 'INPUT') {
								field.setCustomValidity(message);
								field.reportValidity();
							} else {
								field.textContent = message;
							}
						});
					}

					return false;
				});	
			}
		}
	};
})(jQuery);