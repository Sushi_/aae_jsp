'use strict';

(function(api) {
	api.logOut();
	api.redirect('/');
})(api);