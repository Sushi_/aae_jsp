'use strict';

var PlayerState = function(api, doneHandler) {
	var playerControls = document.querySelector('#player_controls');
	var playerControlsThrowDice = document.querySelector('#player_controls_throw_dice');
	var playerControlsGiveDice = document.querySelector('#player_controls_give_dice');
	var playerControlsUseCard = document.querySelector('#player_controls_use_card');
	var select = playerControlsUseCard.querySelector('select[name=cardId]');
	var game_ = null;
	select.addEventListener('change', function() {
		var option = this.options[this.selectedIndex];
		var effectCode = option.getAttribute('effect') >> 0;
		var i;
		
		var selectsToRemove = playerControlsUseCard.querySelectorAll('select');
		for (i = 0; i < selectsToRemove.length; i++) {
			if (selectsToRemove[i].name === 'cardId') {
				continue;
			}
			
			selectsToRemove[i].parentNode.removeChild(selectsToRemove[i]);
		}
		
		var inputToRemove = playerControlsUseCard.querySelector('input[name=params_0]');
		if (inputToRemove !== null) {
			inputToRemove.parentNode.removeChild(inputToRemove);
		}

		// add player select
		if (effectCode === 4 || effectCode === 6 || effectCode === 9) {
			var playerList = game_.players;
			var playerSelect = document.createElement('select');
			playerSelect.name = 'params';
			
			for (i = 0; i < playerList.length; i++) {
				var player = playerList[i];
				if (player.player.id === game_.currentPlayer) {
					continue;
				}
				
				var playerOption = document.createElement('option');
				playerOption.textContent = player.player.username;
				playerOption.value = player.player.id;
				
				playerSelect.appendChild(playerOption);
			}
			
			playerControlsUseCard.insertBefore(playerSelect, select.nextSibling);
		}
		
		// add card select
		else if (effectCode === 5) {
			var pickCardSelect = document.createElement('select');
			pickCardSelect.name = 'params_1';

			playerList = game_.players;
			for (i = 0; i < playerList.length; i++) {
				player = playerList[i];
				if (player.player.id === game_.currentPlayer) {
					continue;
				}
				
				var cardOptGroup = document.createElement('optgroup');
				cardOptGroup.label = player.player.username;
				for (var j = 0; j < player.cards.length; j++) {
					var card = player.cards[j];
					var cardOption = document.createElement('option');
					
					cardOption.setAttribute('uid', player.player.id);
					cardOption.value = card.id;
					cardOption.textContent = card.name;
					
					cardOptGroup.appendChild(cardOption);
				}
				
				pickCardSelect.appendChild(cardOptGroup);
			}

			var uidInput = document.createElement('input');
			uidInput.name = 'params_0';
			uidInput.setAttribute('type', 'hidden');
			
			pickCardSelect.addEventListener('change', function() {
				var option = this.options[this.selectedIndex];
				uidInput.value = option.getAttribute('uid');
			});

		    var evt = document.createEvent('HTMLEvents');
		    evt.initEvent('change', false, true);
		    pickCardSelect.dispatchEvent(evt);
			
			playerControlsUseCard.insertBefore(pickCardSelect, select.nextSibling);
			playerControlsUseCard.appendChild(uidInput);
		}
		
		// add direction select
		else if (effectCode === 2) {
			var directionSelect = document.createElement('select');
			directionSelect.name = 'params';

			var directionOption = document.createElement('option');
			directionOption.textContent = 'Haut en bas';
			directionOption.value = 'clockwise';
			directionSelect.appendChild(directionOption);

			directionOption = document.createElement('option');
			directionOption.textContent = 'Bas en haut';
			directionOption.value = 'anticlockwise';
			directionSelect.appendChild(directionOption);

			playerControlsUseCard.insertBefore(directionSelect, select.nextSibling);
		}
	});
	
	api.bindForm(playerControlsThrowDice, {
		method: 'PUT',
		dataHandler: doneHandler
	});
	
	api.bindForm(playerControlsGiveDice, {
		dataHandler: doneHandler
	});

	api.bindForm(playerControlsUseCard, {
		dataHandler: doneHandler,
		//method: 'PUT',
		preSubmit: function(button, overwrite) {
			if (button.name === 'skip') {
				overwrite.body = 'cardId=-1';
			} else {
				delete overwrite.body;
			} 
			
			return true;
		} 
	});
	
	var states = {
		0: function() { // Waiting
			// Do nothing
		},
		1: function() { // THROWING_DICES
			playerControlsThrowDice.classList.add('active');
		},
		2: function(player, game) { // GIVING_DICES
			var diceToGiveCount = 0;
			for (var i = 0; i < player.dices.length; i++) {
				var dice = player.dices[i];
				if (dice.value === 3) {
					diceToGiveCount++;
				}
			}
			
			playerControlsGiveDice.classList.add('active');
			playerControlsGiveDice.querySelector('span').textContent = diceToGiveCount;
			var select = playerControlsGiveDice.querySelector('select');
			select.innerHTML = '';
			
			for (i = 0; i < game.players.length; i++) {
				var otherPlayer = game.players[i];
				if (otherPlayer.player.id === player.player.id) {
					continue;
				}
				
				var option = document.createElement('option');
				option.setAttribute('value', otherPlayer.player.id);
				option.textContent = otherPlayer.player.username;
				
				select.appendChild(option);
			}
		},
		3: function(player) { // USING_CARD
			
			var wazabiDiceCount = 0;
			for (var i = 0; i < player.dices.length; i++) {
				var dice = player.dices[i];
				if (dice.value === 1) {
					wazabiDiceCount++;
				}
			}
			
			var playableCards = [];
			for (i = 0; i < player.cards.length; i++) {
				var card = player.cards[i];
				if (card.cost <= wazabiDiceCount) {
					playableCards.push(card);
				}
			}
			
			if (playableCards.length === 0) {
				console.log('no card available, skipping');
				playerControlsUseCard.querySelector('button[name=skip]').click();
				return;
			}
			
			playerControlsUseCard.classList.add('active');
			
			select.innerHTML = '';
			for (i = 0; i < playableCards.length; i++) {
				var card = playableCards[i];

				var option = document.createElement('option');
				option.setAttribute('value', card.id);
				option.setAttribute('effect', card.effect_code);
				option.textContent = card.name;

				select.appendChild(option);
			}

			select.selectedIndex = 0;
		    var evt = document.createEvent('HTMLEvents');
		    evt.initEvent('change', false, true);
		    select.dispatchEvent(evt);
		}
	};
	
	this.printControls = function(player, game) {
		game_ = game; // omg
		var activeControls = playerControls.querySelector('.active');
		if (activeControls !== null) {
			activeControls.classList.remove('active');
		}
		
		var stateHandler = states[player.state];
		if (stateHandler === void 0) {
			throw new Error('no state handler is defined for player state ' + player.state);
		}
		
		stateHandler(player, game);
	}
};