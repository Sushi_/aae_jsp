'use strict';

var domifier = (function() {
	return {
		domifyPlayerEntity: function(playerEntity, currentPlayerId, isCurrentPlayer) {
			var domPlayer = document.createElement('div');

			domPlayer.classList.add('player');
			domPlayer.setAttribute('name', playerEntity.player.username);
			domPlayer.setAttribute('playerid', playerEntity.player.id);

			var name = document.createElement('h4');
			name.textContent = playerEntity.player.username;
			
			domPlayer.appendChild(name);
			
			if (playerEntity.cards !== void 0) {
				var cardList = document.createElement('ul');
				cardList.classList.add('cardlist');
				for (var i = 0; i < playerEntity.cards.length; i++) {
					var card = playerEntity.cards[i];
					
					var cardElement = document.createElement('li');
					cardElement.appendChild(this.domifyCardEntity(card, playerEntity, isCurrentPlayer));
					cardList.appendChild(cardElement);
				}
				
				domPlayer.appendChild(cardList);
			}
			
			if (playerEntity.dices !== void 0) {
				var diceList = document.createElement('ul');
				diceList.classList.add('dicelist');
				for (i = 0; i < playerEntity.dices.length; i++) {
					var dice = playerEntity.dices[i];

					var diceElement = document.createElement('li');
					diceElement.appendChild(this.domifyDiceEntity(dice));

					diceList.appendChild(diceElement);
				}

				domPlayer.appendChild(diceList);
			}

			if (playerEntity.player.id === currentPlayerId) {
				domPlayer.classList.add('current_player');
			}

			return domPlayer;
		},
		
		domifyCardEntity: function(cardEntity, cardOwner, visible) {
			var card = document.createElement('div');
			card.classList.add('card');
			card.classList.add('card_type_' + (cardOwner.player.id % 3));

			if (visible) {
				card.classList.add('visible');
				card.classList.add('card_' + cardEntity.effect_code);
				card.setAttribute('name', cardEntity.name);
				card.setAttribute('title', cardEntity.description);
				
				var domName = document.createElement('p');
				domName.classList.add('cardname');
				domName.textContent = cardEntity.name;
				card.appendChild(domName);
				
				var domCost = document.createElement('p');
				domCost.classList.add('cardcost');
				domCost.textContent = 'coût: ' + cardEntity.cost + ' rune wazabi';
				card.appendChild(domCost);
			}

			return card;
		},
		
		domifyDiceEntity: function(diceEntity) {
			var dice = document.createElement('div');
			dice.classList.add('dice');
			dice.classList.add('dice_' + diceEntity.value);
			
			dice.setAttribute('diceid', diceEntity.id);
			
			switch (diceEntity.value) {
				case 1:
					dice.setAttribute('title', 'Rune Wazabi');
					break;
				case 2:
					dice.setAttribute('title', 'Rune Prendre carte');
					break;
				case 3:
					dice.setAttribute('title', 'Rune donner rune');
					break;
			}
			
			return dice;
		}
	};
})();