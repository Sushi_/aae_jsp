'use strict';

/*
 * I am so sorry for you who is about to read this.
 * Please do not judge us.
 * A week is so little time.
 * 
 * Also we went insane writing this
 */
(function(api, domifier, PlayerState) {

	var loggedUser = api.getLoggedUser();
	
	var playerGame = document.querySelector('#player_game');
	var playerList = document.querySelector('#player_list');
	var infoDiv = document.querySelector('#message');
	var title = document.querySelector('#page_title');
	
	function refreshGame() {	
		api.callApi('GET', '/game', 'userId=' + loggedUser.id).done(function(game) {
			if (game == null) {
				api.redirect('/');
				return;
			}
			
			printGame(game);
		});
	}
	
	var playerState = new PlayerState(api, refreshGame);
	
	playerList.classList.add('playerlist');
	function printGame(gameEntity) {
		title.textContent = 'Jeu: ' + gameEntity.name;

		printPlayerList(gameEntity.players, gameEntity);

		if (loggedUser.id == gameEntity.currentPlayer) {
			infoDiv.textContent = 'C\'est à vous de jouer.';
		} else {
			setTimeout(refreshGame, 3000);
			if (gameEntity.currentPlayer === void 0) {
				infoDiv.textContent = 'En attente de joueurs...';
			} else  {
				infoDiv.textContent = 'En attente du début du tour...';
			}
		}
	}

	function printPlayerList(playerEntityList, gameEntity) {
		playerList.innerHTML = '';
		
		var list = document.createElement('ul');
		
		for (var i = 0; i < playerEntityList.length; i++) {
			var player = playerEntityList[i];

			if (player.player.id == loggedUser.id) {
				playerGame.innerHTML = '';
				var domPlayer = domifier.domifyPlayerEntity(player, gameEntity.currentPlayer, true);
				domPlayer.classList.add('logged_player');
				playerGame.appendChild(domPlayer);
				
				playerState.printControls(player, gameEntity);
			} else {
				var element = document.createElement('li');
				element.appendChild(domifier.domifyPlayerEntity(player, gameEntity.currentPlayer, false));

				list.appendChild(element);
			}
		}
		
		playerList.appendChild(list);
	}

	// Forms:
	api.bindForm(document.querySelector('#quit_form'), {
		method: 'DELETE',
		doneHandler: function() {
			api.redirect('/');
		},
	});

	refreshGame();
})(api, domifier, PlayerState);