<%@ page isErrorPage="true"%>

<div class="book closed">
	<div class="page left">
		<section class="book_content">
			<h3>404!</h3>
			<p>Ce n'est pas le livre que vous recherchez.</p>
		</section>
	</div>
</div> 