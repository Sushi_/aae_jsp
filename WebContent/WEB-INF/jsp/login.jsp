<div class="book closed">
	<div class="page">
		<section class="book_content">
			<h3>Connexion</h3>
			<form method="POST" action="login">
				<p>J'appartiens �</p>
				<div class="float_label">
					<label for="login_username">Nom d'utilisateur</label>
					<input type="text" name="username" id="login_username">
				</div> 
				<div class="float_label">
					<label for="login_password">Mot de passe</label>
					<input type="password" name="password" id="login_password">
				</div>
				
				<button type="submit" name="login">Se connecter</button>
				<button type="submit" name="register">S'inscrire</button>
			</form>
		</section>
	</div>
</div>

<script src="assets/js/Login.js" defer></script>