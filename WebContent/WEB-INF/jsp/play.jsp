<div class="book open">
	<div class="page left">
		<section class="book_content">
			<h3>Adversaires</h3>
			<p id="message"></p>
			<div id="player_list">
				
			</div>
		</section>
	</div>
	<div class="page right">
		<section class="book_content">
			<h3 id="page_title">Jeu en cours</h3>
			<div id="player_game">
			</div>
			<div id="player_controls">
				<form method="POST" action="dice" id="player_controls_throw_dice">
					<button type="submit">Secouer les runes</button>
				</form>
				
				<form method="POST" action="dice" id="player_controls_give_dice">
					<p>Vous avez <span></span> rune(s) � donner, choissez un joueur � qui en donner un:</p>
					<select name="targets">
					</select>
					<button type="submit">Donner une rune</button>
				</form>
				
				<form method="POST" action="card" id="player_controls_use_card">
					<p>Vous pouvez utiliser une des cartes suivantes:</p>
					<select name="cardId">
					
					</select>
					<button type="submit" name="use">Utiliser carte</button>
					<button type="submit" name="skip">Finir le tour</button>
				</form>
			</div>
			
			<form method="POST" action="game/player" id="quit_form">
				<button type="submit">Quitter la partie</button>
			</form>
		</section>
	</div>
</div>

<script src="assets/js/game/Domifier.js" defer></script>
<script src="assets/js/game/PlayerState.js" defer></script>
<script src="assets/js/game/Play.js" defer></script>