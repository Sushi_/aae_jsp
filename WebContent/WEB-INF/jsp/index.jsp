<div class="book open">
	<div class="page left">
		<section class="book_content">
			<h3>Liste des parties</h3>
			<ul class="game_list">
			<c:forEach items="${endedGames}" var="game">
				<li >${game.name}
					<ul class="game_player_list">
						<c:forEach items="${game.playerList}" var="player">
							<li>${player.player.username}
								<c:if test="${player.player.id == game.winner.id}">
									(gagnant)
								</c:if>
							</li>
						</c:forEach>
					</ul>
				</li>
			</c:forEach>
			</ul>
			<c:if test="${fn:length(endedGames) == 0}">
				<p>Aucun jeu n'a encore �t� termin�</p>
			</c:if>
		</section>
	</div>
	<div class="page right">
		<section class="book_content">
			<h3>Parties courantes</h3>
			
			<ul>
				<c:forEach items="${currentGames}" var="game">
					<li>${game.name} (${game.playerCount}/${config.minPlayers}): ${game.state == 'INITIATING' ? 'En attente de joueurs' : 'En cours de jeu'}
						<c:if test="${fn:length(game.playerList) != 0}">
							<br>Joueurs:
						</c:if>
						<ul> 
							<c:forEach items="${game.playerList}" var="player">
								<li>${player.player.username}</li>
							</c:forEach>
						</ul>
						<c:if test="${game.state == 'INITIATING'}">
							<form method="post" action="game/player" class="form_join">
								<button type="submit">Rejoindre</button>
								<input type="hidden" name="gameId" value="${game.id}">
							</form>
						</c:if>
					</li>
				</c:forEach>
			</ul>
 
			<c:if test="${fn:length(currentGames) == 0}">
				<p>Aucun jeu n'est actuellement en cours.</p> 
				<h3>Cr�er une nouvelle partie</h3>
				
				<form method="post" action="game" id="form_create">
					<div class="float_label">
						<label for="create_name">Nom de la partie</label>
						<input type="text" name="name" id="create_name">
					</div>
					
					<button type="submit">Cr�er</button>
				</form>
			</c:if>
		</section>
	</div>
</div>

<script src="assets/js/Index.js" defer></script>