package be.ipl.wazabi.web.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.ucc.IGameListUcc;
import be.ipl.wazabi.web.util.HttpUtil;

@WebServlet("/play")
public class PlayServlet extends HttpServlet {

  private static final long serialVersionUID = -7461818569640486778L;

  @EJB
  private IGameListUcc gameListUcc;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    if (!HttpUtil.isLogged(request)) {
      response.sendRedirect(response.encodeRedirectURL("login"));
      return;
    }

    GameEntity currentGame = gameListUcc.getCurrentGame(HttpUtil.getLoggedUserUid(request));
    if (currentGame == null) {
      response.sendRedirect(response.encodeRedirectURL(""));
      return;
    }

    getServletContext().getNamedDispatcher("play.html").forward(request, response);
  }
}
