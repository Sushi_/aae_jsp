package be.ipl.wazabi.web.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.ucc.IGameListUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.util.StringUtil;
import be.ipl.wazabi.web.stringifier.Stringifier;
import be.ipl.wazabi.web.util.HttpUtil;

/**
 * Servlet implementation class CreateGame
 */
@WebServlet("/game")
public class GameServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @EJB
  private IGameListUcc ucc;

  @EJB
  private Stringifier stringifier;

  /**
   * Returns depends on the query param sent: 
   * - query param userId: sends the game the user with that user id is in. 
   * - query param gameId: sends the game with that id.
   * - none: sends the list of active games.
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String uidStr = req.getParameter("userId");
    String gidStr = req.getParameter("gameId");

    if (!StringUtil.isEmpty(uidStr)) {
      int uid = StringUtil.isNumeric(uidStr) ? Integer.parseInt(uidStr) : -1;

      GameEntity game = ucc.getCurrentGame(uid);
      resp.getWriter().println(stringifier.stringify(game));
    } else if (!StringUtil.isEmpty(gidStr)) {
      int gid = StringUtil.isNumeric(gidStr) ? Integer.parseInt(gidStr) : -1;

      GameEntity game = ucc.getGameById(gid);
      resp.getWriter().println(stringifier.stringify(game));
    } else {
      resp.getWriter().println(stringifier.stringify(ucc.listActiveGames()));
    }
  }

  /**
   * Creates a new game and returns it. See {@link IGameListUcc#createGame(String)} for possible
   * errnos.
   * 
   * @queryParameter: name the name of the game.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!HttpUtil.isLogged(request)) {
      response.setStatus(401);
      response.getWriter().write("{ error: \"login required\" }");
      return;
    }
    
    String name = request.getParameter("name");
    try {
      GameEntity game = ucc.createGame(name);
      ucc.joinGame(game.getId(), HttpUtil.getLoggedUserUid(request));
      response.getWriter().write(stringifier.stringify(game));
    } catch (UccException e) {
      response.setStatus(400);
      response.getWriter().write(stringifier.stringify(e));
    }
  }
}
