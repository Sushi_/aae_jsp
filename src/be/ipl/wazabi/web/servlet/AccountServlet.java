package be.ipl.wazabi.web.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.ucc.IUserUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.util.StringUtil;
import be.ipl.wazabi.web.stringifier.Stringifier;

@WebServlet("/account")
public class AccountServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @EJB
  private IUserUcc userUcc;

  @EJB
  private Stringifier stringifier;

  /**
   * Returns the details of the user with a given ID.
   * @queryParameters uid The id of the user to fetch.
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String uidStr = req.getParameter("uid");
    int uid = StringUtil.isNumeric(uidStr) ? Integer.parseInt(uidStr) : -1;

    resp.getWriter().println(stringifier.stringify(userUcc.getById(uid)));
  }

  /**
   * Creates a new account.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String username = request.getParameter("username");
    String password = request.getParameter("password");

    try {
      PlayerEntity player = userUcc.register(username, password);

      HttpSession session = request.getSession();
      synchronized (session) {
        session.setAttribute("uid", player.getId());
      }

      response.getWriter().println(stringifier.stringify(player));
    } catch (UccException e) {
      response.setStatus(400);
      response.getWriter().println(stringifier.stringify(e));
    }
  }
}
