package be.ipl.wazabi.web.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.web.stringifier.Stringifier;
import be.ipl.wazabi.web.util.HttpUtil;

/**
 * Servlet implementation class DiceServlet
 */
@WebServlet("/dice")
public class DiceServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @EJB
  private IGameUcc ucc;

  @EJB
  private Stringifier stringifier;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    if (!HttpUtil.isLogged(req)) {
      resp.setStatus(401);
      resp.getWriter().write(stringifier.stringify("{ error: \"login required\" }"));
      return;
    }
    
    HttpSession session = req.getSession();
    int uid;
    synchronized (session) {
      uid = (int) session.getAttribute("uid");
    }

    try {
      ucc.getDiceCount(uid);
    } catch (UccException e) {
      resp.setStatus(400);
      resp.getWriter().write(stringifier.stringify(e));
    }
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    
    if (!HttpUtil.isLogged(request)) {
      response.setStatus(401);
      response.getWriter().write(stringifier.stringify("{ error: \"login required\" }"));
      return;
    }
    
    String[] destinationsUserStrs = request.getParameterValues("targets");
    int[] destinationUserIds = new int[destinationsUserStrs.length];
    
    for (int i = 0; i < destinationsUserStrs.length; i++) {
      try {
        destinationUserIds[i] = Integer.parseInt(destinationsUserStrs[i]);
      } catch (NumberFormatException e) {
        destinationUserIds[i] = -1;
      }
    }

    try {
      ucc.giveDices(HttpUtil.getLoggedUserUid(request), destinationUserIds);
      response.getWriter().write("{}");
    } catch (UccException e) {
      response.setStatus(400);
      response.getWriter().write(stringifier.stringify(e));
    }

  }

  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    
    if (!HttpUtil.isLogged(req)) {
      resp.setStatus(401);
      resp.getWriter().write(stringifier.stringify("{ error: \"login required\" }"));
      return;
    }
    
    HttpSession session = req.getSession();
    int uid;
    synchronized (session) {
      uid = (int) session.getAttribute("uid");
    }

    try {
      resp.getWriter().write(stringifier.stringify(ucc.rollDices(uid)));
    } catch (UccException e) {
      resp.setStatus(400);
      resp.getWriter().write(stringifier.stringify(e));
    }
  }

}
