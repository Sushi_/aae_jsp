package be.ipl.wazabi.web.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.ucc.IUserUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.web.stringifier.Stringifier;
import be.ipl.wazabi.web.util.HttpUtil;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @EJB
  private IUserUcc userUcc;

  @EJB
  private Stringifier stringifier;

  /**
   * Sends the login html page to the user.
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    if (HttpUtil.isLogged(request)) {
      response.sendRedirect(response.encodeRedirectURL("index"));
      return;
    }

    getServletContext().getNamedDispatcher("login.html").forward(request, response);
  }

  /**
   * Connects the user, sends a json response.
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String username = request.getParameter("username");
    String password = request.getParameter("password");

    try {
      PlayerEntity player = userUcc.logIn(username, password);

      HttpSession session = request.getSession();
      synchronized (session) {
        session.setAttribute("uid", player.getId());
      }

      response.getWriter().println(stringifier.stringify(player));
    } catch (UccException e) {
      response.setStatus(400);
      response.getWriter().println(stringifier.stringify(e));
    }
  }
}
