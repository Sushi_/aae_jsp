package be.ipl.wazabi.web.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.ipl.wazabi.ucc.IGameListUcc;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.web.stringifier.Stringifier;
import be.ipl.wazabi.web.util.HttpUtil;

/**
 * Servlet implementation class CreateGame
 */
@WebServlet("/game/player")
public class GameJoinServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @EJB
  private IGameListUcc ucc;

  @EJB
  private IGameUcc gameUcc;
  
  @EJB
  private Stringifier stringifier;

  /**
   * Adds the logged player to the requested game. See {@link IGameListUcc#joinGame(int, int)} for
   * possible errnos.
   *
   * @queryParameter: gameId the id of the game to join
   */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    if (!HttpUtil.isLogged(req)) {
      resp.setStatus(401);
      resp.getWriter().write("{ error: \"login required\" }");
      return;
    }

    String gid = req.getParameter("gameId");
    try {
      ucc.joinGame(Integer.parseInt(gid), HttpUtil.getLoggedUserUid(req));
      resp.getWriter().write("{}");
    } catch (UccException e) {
      resp.setStatus(400);
      resp.getWriter().write(stringifier.stringify(e));
    } catch (NumberFormatException e) {
      resp.setStatus(400);
    }
  }
  
  /**
   * Removes the logged player from its current game. See {@link IGameListUcc#quitGame(int)} for
   * possible errnos.
   */
  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    if (!HttpUtil.isLogged(req)) {
      resp.setStatus(401);
      resp.getWriter().write("{ error: \"login required\" }");
      return;
    }

    try {
      gameUcc.quitGame(HttpUtil.getLoggedUserUid(req));
      resp.getWriter().write("{}");
    } catch (UccException e) {
      resp.setStatus(400);
      resp.getWriter().write(stringifier.stringify(e));
    }
  }
}
