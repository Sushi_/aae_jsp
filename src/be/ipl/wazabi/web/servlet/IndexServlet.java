package be.ipl.wazabi.web.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GameEntity.State;
import be.ipl.wazabi.main.Setup;
import be.ipl.wazabi.ucc.IGameListUcc;
import be.ipl.wazabi.web.util.HttpUtil;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {

  private static final long serialVersionUID = -7461818569640486778L;

  @EJB
  private IGameListUcc gameListUcc;

  @EJB
  private Setup config;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    if (!HttpUtil.isLogged(request)) {
      response.sendRedirect(response.encodeRedirectURL("login"));
      return;
    }

    GameEntity currentGame = gameListUcc.getCurrentGame(HttpUtil.getLoggedUserUid(request));
    if (currentGame != null) {
      response.sendRedirect(response.encodeRedirectURL("play"));
      return;
    }

    request.setAttribute("endedGames", gameListUcc.listGamesByState(State.ENDED));
    request.setAttribute("currentGames", gameListUcc.listActiveGames());
    request.setAttribute("config", config);

    getServletContext().getNamedDispatcher("index.html").forward(request, response);
  }
}
