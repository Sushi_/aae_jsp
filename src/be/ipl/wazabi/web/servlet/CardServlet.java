package be.ipl.wazabi.web.servlet;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.web.stringifier.Stringifier;
import be.ipl.wazabi.web.util.HttpUtil;

/**
 * Servlet implementation class Card
 */
@WebServlet("/card")
public class CardServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @EJB
  private IGameUcc ucc;

  @EJB
  private Stringifier stringifier;

  /**
   * Returns the deck of cards of a given user. See {@link IGameUcc#getCards(int)} for possible
   * errnos.
   * 
   * @queryParameter uid the id of the user.
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    int uid = Integer.parseInt(req.getParameter("uid"));

    try {
      stringifier.stringify(ucc.getCards(uid));
    } catch (UccException e) {
      resp.setStatus(400);
      resp.getWriter().write(stringifier.stringify(e));
    }
  }

  /**
   * Use one of the cards of the logged user. See {@link IGameUcc#playCard(int, int, String...)} for
   * possible errnos.
   * 
   * @queryParameter cardId the id of the user.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    if (!HttpUtil.isLogged(request)) {
      response.setStatus(401);
      response.getWriter().write("{ \"error\": \"login required\" }");
      return;
    }

    try {
      String[] params = request.getParameterValues("params");
      if (params == null) {
        Map<String, String[]> paramMap = request.getParameterMap();

        params = new String[paramMap.size() - 1];
        for (Entry<String, String[]> entry : paramMap.entrySet()) {
          String key = entry.getKey();
          if (!key.startsWith("params_")) {
            continue;
          }
          
          String[] keyParts = key.split("_");
          if (keyParts.length != 2) {
            continue;
          }
          
          params[Integer.parseInt(keyParts[1])] = entry.getValue()[0];
        }
      }

      try {
        ucc.playCard(HttpUtil.getLoggedUserUid(request),
            Integer.parseInt(request.getParameter("cardId")), params);
        response.getWriter().write("{}");
      } catch (UccException e) {
        response.setStatus(400);
        response.getWriter().write(stringifier.stringify(e));
      }
    } catch (NumberFormatException e) {
      response.setStatus(400);
      response.getWriter().write("{ \"error\": \"Invalid number\" }");
    }
  }
}
