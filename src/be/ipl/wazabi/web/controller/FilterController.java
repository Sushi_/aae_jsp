package be.ipl.wazabi.web.controller;

import java.io.IOException;

import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import be.ipl.wazabi.web.stringifier.Stringifier;

@WebFilter(filterName = "FiltreControleur", urlPatterns = "/*",
    dispatcherTypes = {DispatcherType.REQUEST})
public class FilterController implements Filter {

  @EJB
  private Stringifier stringifier;

  public void init(FilterConfig fConfig) throws ServletException {}

  public void destroy() {}

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    if (!(request instanceof HttpServletRequest)) {
      response.getWriter().println("This website is only available through HTTP.");
      return;
    }

    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;

    /*String url = req.getRequestURL().toString();
    if (!req.isRequestedSessionIdFromCookie()) {
      if (url.endsWith("/login")) {
        sendError(req, res, "This website requires cookies to work properly.", 400);
      } else {
        res.sendRedirect(res.encodeRedirectURL("login"));
      }
    }*/

    try {
      chain.doFilter(request, response);
    } catch (Throwable e) {
      e.printStackTrace();
      sendError(req, res, "Internal server error", 500);
    }
  }

  private void sendError(HttpServletRequest request, HttpServletResponse response, String message,
      int httpcode) throws IOException {
    String accept = request.getHeader("Accept");
    if (accept != null && accept.endsWith("json")) {
      // Send the error as json if it's an AJAJ request.
      response.setStatus(httpcode);

      JsonObjectBuilder builder = Json.createObjectBuilder();
      builder.add("message", message);
      builder.add("errno", -1);

      response.getWriter().write(builder.build().toString());
    } else {
      // Send as HTML otherwise.
      request.setAttribute("error", message);
      response.sendError(httpcode);
    }
  }
}
