package be.ipl.wazabi.web.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public final class HttpUtil {

  public static boolean isLogged(HttpServletRequest request) {
    HttpSession session = request.getSession();
    synchronized (session) {
      return session.getAttribute("uid") != null;
    }
  }
  
  public static int getLoggedUserUid(HttpServletRequest request) {
    HttpSession session = request.getSession();
    synchronized (session) {
      Object uid = session.getAttribute("uid");
      if (uid == null) {
        return -1;
      }
      
      return (int) uid;
    }
  }
}
