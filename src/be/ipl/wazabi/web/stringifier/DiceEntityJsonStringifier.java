package be.ipl.wazabi.web.stringifier;

import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import be.ipl.wazabi.entity.DiceEntity;

@Singleton(name = "diceStringifier")
class DiceEntityJsonStringifier implements IPojoJsoner<DiceEntity> {

  @Override
  public JsonValue toJson(DiceEntity entity) {
    JsonObjectBuilder builder = Json.createObjectBuilder();

    builder.add("id", entity.getId());
    builder.add("value", entity.getValue());

    return builder.build();
  }
}
