package be.ipl.wazabi.web.stringifier;

import javax.ejb.Local;
import javax.json.JsonValue;

@Local
public interface IPojoJsoner<E> {
  JsonValue toJson(E entity);
}
