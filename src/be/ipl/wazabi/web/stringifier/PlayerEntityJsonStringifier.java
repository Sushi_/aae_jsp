package be.ipl.wazabi.web.stringifier;

import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import be.ipl.wazabi.entity.PlayerEntity;

@Singleton(name = "playerStringifier")
class PlayerEntityJsonStringifier implements IPojoJsoner<PlayerEntity> {

  @Override
  public JsonValue toJson(PlayerEntity entity) {
    JsonObjectBuilder builder = Json.createObjectBuilder();

    builder.add("id", entity.getId());
    builder.add("username", entity.getUsername());
    // skip password.
    
    return builder.build();
  }
}
