package be.ipl.wazabi.web.stringifier;

import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import be.ipl.wazabi.ucc.exception.UccException;

@Singleton(name = "errorStringifier")
class UccErrorStringifier implements IPojoJsoner<UccException> {

  @Override
  public JsonValue toJson(UccException entity) {
    JsonObjectBuilder builder = Json.createObjectBuilder();

    builder.add("errno", entity.getErrno());
    builder.add("message", entity.getMessage());
    
    return builder.build();
  }
}