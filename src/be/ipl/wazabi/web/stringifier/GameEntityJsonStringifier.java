package be.ipl.wazabi.web.stringifier;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import be.ipl.wazabi.entity.GameEntity;

class GameEntityJsonStringifier implements IPojoJsoner<GameEntity> {
  
  private final Stringifier stringifier;
  
  public GameEntityJsonStringifier(Stringifier stringifier) {
    this.stringifier = stringifier;
  }

  @Override
  public JsonValue toJson(GameEntity entity) {
    JsonObjectBuilder builder = Json.createObjectBuilder();

    builder.add("id", entity.getId());
    builder.add("name", entity.getName());
    builder.add("state", entity.getState().ordinal());

    builder.add("players", stringifier.jsonifyCollection(entity.getPlayerList()));

    if (entity.getState().equals(GameEntity.State.PLAYING)) {
      builder.add("currentPlayer", entity.getCurrentPlayer().getPlayer().getId());
    }

    if (entity.getState().equals(GameEntity.State.ENDED)) {
      builder.add("winner", entity.getWinner().getId());
    }

    return builder.build();
  }
}
