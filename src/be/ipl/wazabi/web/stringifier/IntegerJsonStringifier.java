package be.ipl.wazabi.web.stringifier;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.json.JsonNumber;
import javax.json.JsonValue;

public class IntegerJsonStringifier implements IPojoJsoner<Integer> {

  @Override
  public JsonValue toJson(Integer entity) {
    return new JsonNumberImpl(entity);
  }

  private static class JsonNumberImpl implements JsonNumber {

    private final int value;

    public JsonNumberImpl(int value) {
      this.value = value;
    }

    @Override
    public ValueType getValueType() {
      return JsonValue.ValueType.NUMBER;
    }

    @Override
    public boolean isIntegral() {
      return false;
    }

    @Override
    public int intValue() {
      return value;
    }

    @Override
    public int intValueExact() {
      return value;
    }

    @Override
    public long longValue() {
      return (long) value;
    }

    @Override
    public long longValueExact() {
      return (long) value;
    }

    @Override
    public BigInteger bigIntegerValue() {
      return BigInteger.valueOf(value);
    }

    @Override
    public BigInteger bigIntegerValueExact() {
      return BigInteger.valueOf(value);
    }

    @Override
    public double doubleValue() {
      return value;
    }

    @Override
    public BigDecimal bigDecimalValue() {
      return BigDecimal.valueOf(value);
    }
  }
}
