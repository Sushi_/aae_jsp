package be.ipl.wazabi.web.stringifier;

import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import be.ipl.wazabi.entity.GamePlayerEntity;

@Singleton(name = "playerStringifier")
class GamePlayerEntityJsonStringifier implements IPojoJsoner<GamePlayerEntity> {

  private final Stringifier stringifier;
  
  public GamePlayerEntityJsonStringifier(Stringifier stringifier) {
    this.stringifier = stringifier;
  }
  
  @Override
  public JsonValue toJson(GamePlayerEntity entity) {
    JsonObjectBuilder builder = Json.createObjectBuilder();

    builder.add("dices", stringifier.jsonify(entity.getPlayableDices()));
    builder.add("cards", stringifier.jsonify(entity.getPlayableCards()));
    builder.add("player", stringifier.jsonify(entity.getPlayer()));
    builder.add("state", entity.getState().ordinal());

    return builder.build();
  }
}
