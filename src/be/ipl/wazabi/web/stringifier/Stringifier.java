package be.ipl.wazabi.web.stringifier;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonValue;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

@Singleton
@LocalBean
public class Stringifier {

  private final Map<Class<?>, IPojoJsoner<?>> stringifiers = new HashMap<>();

  public Stringifier() {
    addStringifier(CardEntity.class, new CardEntityJsonStringifier());
    addStringifier(DiceEntity.class, new DiceEntityJsonStringifier());
    addStringifier(GameEntity.class, new GameEntityJsonStringifier(this));
    addStringifier(GamePlayerEntity.class, new GamePlayerEntityJsonStringifier(this));
    addStringifier(PlayerEntity.class, new PlayerEntityJsonStringifier());
    addStringifier(UccException.class, new UccErrorStringifier());
    addStringifier(Integer.class, new IntegerJsonStringifier());
  }

  public <E> void addStringifier(Class<E> entity, IPojoJsoner<E> stringifier) {
    stringifiers.put(entity, stringifier);
  }

  @SuppressWarnings("unchecked")
  protected <E> JsonValue jsonify(E entity) {
    if (entity == null) {
      return JsonValue.NULL;
    }
    
    if (entity instanceof Collection) {
      return jsonifyCollection((Collection<E>) entity);
    }

    IPojoJsoner<E> stringifier = (IPojoJsoner<E>) stringifiers.get(entity.getClass());
    if (stringifier == null) {
      throw new UnsupportedOperationException("No stringifier for entity type " + entity.getClass());
    }
    
    return stringifier.toJson(entity);
  }
  
  @SuppressWarnings("unchecked")
  protected <E> JsonValue jsonifyCollection(Collection<E> entities) {
    JsonArrayBuilder builder = Json.createArrayBuilder();

    if (entities.size() != 0) {
      Iterator<E> iterator = entities.iterator();
      E entity = iterator.next();
      IPojoJsoner<E> stringifier = (IPojoJsoner<E>) stringifiers.get(entity.getClass());
      if (stringifier == null) {
        throw new UnsupportedOperationException("No stringifier for entity type " + entity.getClass());
      }

      builder.add(stringifier.toJson(entity));
      while (iterator.hasNext()) {
        builder.add(stringifier.toJson(iterator.next()));
      }
    }

    return builder.build();
  }

  public <E> String stringify(E entity) {
    return jsonify(entity).toString();
  }
}
