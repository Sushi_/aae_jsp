package be.ipl.wazabi.web.stringifier;

import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import be.ipl.wazabi.entity.CardEntity;

@Singleton(name = "cardStringifier")
class CardEntityJsonStringifier implements IPojoJsoner<CardEntity> {

  @Override
  public JsonValue toJson(CardEntity entity) {
    JsonObjectBuilder builder = Json.createObjectBuilder();

    builder.add("id", entity.getId());
    builder.add("effect_code", entity.getEffect().getCode());
    builder.add("description", entity.getEffect().getDescription());
    builder.add("name", entity.getEffect().getName());
    builder.add("cost", entity.getEffect().getUseCost());

    return builder.build();
  }
}
